import {useRef, useState} from 'react';
import {
  SafeAreaView,
  StatusBar,
  View,
  Text,
  TextInput,
  TouchableHighlight,
  Alert,
  Platform,
} from 'react-native';
import Icon from '../components/Icon';
import {StyleAdapter} from '../components/StyleAdapter';

export default () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [eyeOpen, setEyeOpen] = useState(false);

  const usernameInput = useRef<TextInput>(null);
  const passwordInput = useRef<TextInput>(null);

  const buttonDisabled = !username || password.length < 6;
  const isAndroid = Platform.OS === 'android';

  const clickDone = () => {
    passwordInput.current?.blur();
    clickLogin();
  };
  const clickLogin = () => {
    if (buttonDisabled) return;

    usernameInput.current?.blur();
    passwordInput.current?.blur();

    request(
      'https://mock.apifox.com/m1/4102536-0-default/demo/login',
      {username, password},
      () => Alert.alert('登录成功'),
      (e = '') => Alert.alert(e),
    );
  };

  const request = (
    url = '',
    params = {},
    resolve: {(arg?: any): any},
    reject: {(arg?: string): any},
  ) => {
    const option = {
      method: 'POST',
      type: 'json',
      body: JSON.stringify(params || {}),
      headers: {'Content-Type': 'application/json'},
    };

    // @ts-ignore
    fetch(url, option)
      .then(response => response.json())
      .then(data => {
        if (data.success) {
          resolve && resolve(data.data);
        } else {
          reject && reject(data.desc);
        }
      })
      .catch(e => {
        reject && reject(e.message);
      });
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={styles.container.backgroundColor}
      />
      <View style={styles.container}>
        <Icon style={styles.navLeft} src="icon_back" />
        <Text style={styles.navRight}>预约开通</Text>

        <Icon style={styles.logo} src="logo_sqb" />

        <View style={styles.inputBg}>
          <TextInput
            ref={usernameInput}
            style={[styles.input, username ? styles.inputValue : {}]}
            placeholder="请输入手机号/邮箱"
            placeholderTextColor="#999999"
            keyboardType={isAndroid ? 'numeric' : 'numbers-and-punctuation'}
            returnKeyType="next"
            value={username}
            onChangeText={text => setUsername(text)}
            onSubmitEditing={() => passwordInput.current?.focus()}
          />
          {username && (
            <Icon
              style={styles.s30}
              src="icon_close"
              onPress={() => setUsername('')}
            />
          )}
        </View>

        <View style={styles.inputBg}>
          <TextInput
            ref={passwordInput}
            style={[styles.input, password ? styles.inputValue : {}]}
            placeholder="请输入登录密码"
            placeholderTextColor="#999999"
            secureTextEntry={!eyeOpen}
            value={password}
            onChangeText={text => setPassword(text)}
            onSubmitEditing={clickDone}
          />
          {password && (
            <Icon
              style={styles.s30}
              src="icon_close"
              onPress={() => setPassword('')}
            />
          )}
          <Icon
            style={[styles.s30, styles.eye]}
            src={eyeOpen ? 'icon_eye_1' : 'icon_eye_0'}
            onPress={() => setEyeOpen(!eyeOpen)}
          />
          <View style={styles.separator} />
          <Text style={styles.tip}>忘记密码</Text>
        </View>

        <TouchableHighlight
          style={[styles.button, buttonDisabled ? styles.buttonDisabled : {}]}
          underlayColor={styles.buttonPressed.backgroundColor}
          disabled={buttonDisabled}
          onPress={clickLogin}>
          <Text style={styles.buttonText}>登录</Text>
        </TouchableHighlight>
        <Text style={[styles.tip, styles.verify]}>手机验证码登录</Text>

        <View style={{flex: 1}} />
        <View style={styles.bottom}>
          <View style={styles.third}>
            <Icon style={styles.thirdIcon} src="icon_wechat" />
            <Text style={styles.thirdText}>微信登录</Text>
          </View>
          <View style={styles.third}>
            <Icon style={styles.thirdIcon} src="icon_alipay" />
            <Text style={styles.thirdText}>支付宝登录</Text>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleAdapter.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  navLeft: {
    position: 'absolute',
    top: 38,
    left: 32,
    width: 18,
    height: 30,
  },
  navRight: {
    fontSize: 26,
    color: '#666666',
    fontWeight: '600',
    position: 'absolute',
    top: 40,
    right: 32,
  },
  logo: {
    width: 300,
    height: 95,
    alignSelf: 'center',
    marginTop: 219,
    marginBottom: 90,
  },
  inputBg: {
    marginTop: 30,
    marginHorizontal: 64,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#e5E5E5',
    height: 99,
  },
  input: {
    fontSize: 28,
    color: '#000000',
    flex: 1,
    height: '100%',
  },
  inputValue: {
    fontSize: 36,
  },
  s30: {
    width: 30,
    height: 30,
  },
  eye: {
    marginLeft: 12,
  },
  separator: {
    width: 2,
    height: 26,
    backgroundColor: '#e5E5E5',
    marginHorizontal: 22,
  },
  tip: {
    fontSize: 26,
    color: '#666666',
  },
  button: {
    marginTop: 63,
    marginHorizontal: 64,
    borderRadius: 45,
    backgroundColor: '#ee9E00',
  },
  buttonText: {
    fontSize: 32,
    color: '#ffffff',
    textAlign: 'center',
    lineHeight: 88,
  },
  buttonDisabled: {
    backgroundColor: '#f5e2b8',
  },
  buttonPressed: {
    backgroundColor: '#b5802B',
  },
  verify: {
    marginTop: 32,
    marginHorizontal: 64,
  },
  bottom: {
    flexDirection: 'row',
    paddingHorizontal: 64,
    paddingBottom: 188,
  },
  third: {
    flex: 1,
    alignItems: 'center',
  },
  thirdIcon: {
    width: 90,
    height: 90,
    borderRadius: 45,
    borderWidth: 1,
    borderColor: '#e5E5E5',
  },
  thirdText: {
    fontSize: 22,
    color: '#999999',
    marginTop: 12,
  },
});
