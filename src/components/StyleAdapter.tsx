import {Dimensions, StyleSheet} from 'react-native';

export namespace StyleAdapter {
  type NamedStyles<T> = StyleSheet.NamedStyles<T>;

  export function create<T extends NamedStyles<T> | NamedStyles<any>>(
    styles: T | NamedStyles<T>,
  ): T {
    const adaptSize = (size = 0) =>
      (size / 750) * Dimensions.get('window').width;

    for (const k1 in styles) {
      for (const k2 in styles[k1]) {
        if (
          typeof styles[k1][k2] === 'number' &&
          [
            'top',
            'right',
            'bottom',
            'left',
            'width',
            'height',
            'size',
            'margin',
            'padding',
            'radius',
          ].find(v => k2.toLocaleLowerCase().includes(v))
        ) {
          // @ts-ignore
          styles[k1][k2] = adaptSize(styles[k1][k2]); // 对数字类型的样式进行适配处理
        }
      }
    }

    return StyleSheet.create(styles);
  }
}
