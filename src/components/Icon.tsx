import {Pressable, Image} from 'react-native';

export default ({style = {}, src = '', onPress = () => {}}) => (
  <Pressable style={style} onPress={onPress}>
    <Image style={{flex: 1}} source={{uri: src}} />
  </Pressable>
);
