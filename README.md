### 步骤

1. 安装weex-cli，创建并启动项目

```
node版本需要V14以上
npx react-native init DemoReactNative
```

2. 运行iOS

```
cd ios
pod install
yarn ios
```

3. 运行安卓

```
yarn android
```

4. 账号密码

```
10086
111111
```

### 注意

- `yarn android`失败

```
jdk版本需要11及以上
```

- 尺寸适配屏幕

```
见StyleAdapter.tsx
```
